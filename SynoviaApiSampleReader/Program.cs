﻿using System;
using System.Threading;
using System.Xml;
using SynoviaApiSampleReader.Synovia;

namespace SynoviaApiSampleReader
{
    class Program
    {
        private const string _custGuid = "ED08CC6A-53F3-4914-9D3B-862350B79C5D";
        private static ISynoviaApi _svc;

        static void Main(string[] args)
        {
            _svc = new SynoviaApiClient("BasicHttpBinding_ISynoviaApi",
                "https://api.synovia.com/SynoviaApi.svc");

            GetZoneHistory("0", "2018-12-01T00:00:00", "0");
        }

        private static void GetZoneHistory(string startId, string startDate, string lastStartId)
        {
            var results = _svc.s0107(_custGuid, startId, startDate, "", "", "", "", "0");
            var nextCallOffset = "0";
            var nextId = "0";

            using (var reader = results.CreateReader())
            {
                while (reader.Read())
                {
                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        if (reader.Name == "Status")
                        {
                            var status = reader.GetAttribute("Code");
                            if (status != "0" && status != "1" && status != "2")
                            {
                                // you should handle API returning an error message here
                                return;
                            }

                            nextCallOffset = reader.GetAttribute("NextCallTime");
                            nextId = reader.GetAttribute("NextID");
                        }
                    }

                    // Parse results to get data                    
                }
            }

            // If you have read all of the data for the timeframe you want, return 

            if (nextId == lastStartId)
            {
                // no new data since last execution, sleep for X minutes
                Thread.Sleep(TimeSpan.FromMinutes(5));
            }
            else
            {
                Thread.Sleep(TimeSpan.FromSeconds(Convert.ToDouble(nextCallOffset)));
            }

            GetZoneHistory(nextId, "", startId);            
        }
    }
}
